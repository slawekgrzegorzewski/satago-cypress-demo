describe('Conduit page test suite', { baseUrl: 'https://react-redux.realworld.io/' }, () => {

    //this is executed before every "it" inside the suite
    //it sets up the token since every test resets the local storage
    beforeEach(() => {
        cy.request({
            url: 'https://api.realworld.io/api/users/login',
            method: 'POST',
            body: {
                user: {
                    email: Cypress.env('login'),
                    password: Cypress.env('password')
                }
            }
        })
            .its('body')
            .then(response => localStorage.setItem('jwt', response.user.token))
    })

    //this test check if the navigation element contains 
    //the username which is an indicator of logged user
    it('logs in without the UI', () => {
        cy.visit('/')
        cy.get('ul.navbar-nav li').last().should('include.text', Cypress.env('username'))
    })

    //this filters out the articles based on "et" tag and checks if
    //the result list contains the same tag in every article
    it('filters the articles using tags', () => {
        cy.visit('/')
        cy.get('div.tag-list').contains('et').click()
        cy.get('div.article-preview ul.tag-list').each($el => {
            cy.wrap($el).find('li').contains('et').should('exist')
        })
    })

})