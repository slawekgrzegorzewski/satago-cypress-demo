import Homepage_PO from "../support/page-objects/Homepage_PO"

describe('WebDriverUniversity page test suite', { baseUrl: 'http://webdriveruniversity.com/' }, () => {

    const homepage = new Homepage_PO

    beforeEach(() => {
        homepage.navigate('/')
    })

    it("contains 18 headers", () => {
        cy.get('h1').should('have.length', 18)
    })

})