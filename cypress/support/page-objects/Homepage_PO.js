/// <reference types="cypress" />

class Homepage_PO {

    elements = {
        homepageHeaders: () => cy.get('h1')
    }

    navigate(path) {
        cy.visit(path)
    }

    getElement(element) {
        return this.elements.element
    }
    
}

export default Homepage_PO;